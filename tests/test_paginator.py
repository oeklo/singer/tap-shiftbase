import datetime as dt

from tap_shiftbase.paginator import iterate_period


def test_get_next():
    iter_ = iterate_period(dt.date.today(), dt.date.today())
    next(iter_)
