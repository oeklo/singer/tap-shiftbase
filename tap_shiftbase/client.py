"""REST client handling, including ShiftbaseStream base class."""

from __future__ import annotations

from pathlib import Path
from typing import Callable, Generic

import requests
from singer_sdk.authenticators import SimpleAuthenticator
from singer_sdk.streams import RESTStream
from singer_sdk.streams.rest import _TToken

_Auth = Callable[[requests.PreparedRequest], requests.PreparedRequest]
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class ShiftbaseStream(RESTStream, Generic[_TToken]):
    """Shiftbase stream class."""

    url_base = "https://api.shiftbase.com/api"

    @property
    def authenticator(self) -> _Auth:
        return SimpleAuthenticator(self, {'Authorization': self.config['auth_token']})
