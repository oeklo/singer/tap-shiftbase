import datetime as dt
from collections.abc import Iterator


def iterate_period(start_date: dt.date, end_date: dt.date) -> Iterator[tuple[dt.date, dt.date]]:
    assert start_date <= end_date, f'{start_date.isoformat()} <= {end_date.isoformat()}'

    current_date = start_date
    while current_date < end_date:
        if current_date.replace(day=1) != end_date.replace(day=1):
            # Start of a month
            next_month_ = next_month(current_date)
            end_of_month = next_month_ - dt.timedelta(days=1)

            yield current_date, end_of_month
            current_date = next_month_
        else:
            yield current_date, end_date
            current_date = end_date


def next_month(date: dt.date) -> dt.date:
    return (date.replace(day=1) + dt.timedelta(days=32)).replace(day=1)
