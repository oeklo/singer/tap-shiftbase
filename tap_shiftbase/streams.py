"""Stream type classes for tap-shiftbase."""

from __future__ import annotations

import datetime as dt
from collections.abc import Iterable
from pathlib import Path
from typing import Any

import requests
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams.rest import _TToken

from tap_shiftbase.client import ShiftbaseStream
from tap_shiftbase.paginator import iterate_period

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class RosterStream(ShiftbaseStream[tuple[dt.date, dt.date]]):
    name = 'roster'
    path = '/rosters'
    schema_filepath = SCHEMAS_DIR / 'roster.json'
    primary_keys = ['id']
    _shifts = {}

    def get_records(self, context: dict | None) -> Iterable[dict | tuple[dict, dict]]:
        records = list(self.request_records(context))
        RosterStream._shifts.update({item['Shift']['id']: item['Shift'] for item in records})
        for record in records:
            transformed_record = self.post_process(record, context)
            if transformed_record is None:
                # Record filtered out during post_process()
                continue
            yield transformed_record, {'shift_id': transformed_record['shift_id']}

    def request_records(self, context: dict | None) -> Iterable[dict]:
        state = self.get_context_state(context)

        date_min = dt.date.fromisoformat(state['date_max']) if 'date_max' in state else dt.date.fromisoformat(self.config['start_date'])
        for period in iterate_period(date_min, dt.date.today()):
            subcontext = {'date_min': period[0], 'date_max': period[1]}
            if context:
                subcontext.update(context)
            yield from super().request_records(subcontext)
            state['date_max'] = period[1]

    def get_url_params(self, context: dict | None, next_page_token: tuple[dt.date, dt.date] | None) -> dict[str, Any]:
        self.logger.info('next page token: %s', next_page_token)
        if not next_page_token:
            return {}
        return {
            'min_date': next_page_token[0],
            'max_date': next_page_token[1],
        }

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        # Return the parent object to keep the side-loaded children
        yield from extract_jsonpath('$.data[*]', input=response.json())

    def post_process(
            self,
            row: dict,
            context: dict | None = None,  # noqa: ARG002
    ) -> dict | None:
        return row['Roster']


# def parse_route_number(descr: str) -> int | None:
#     matches = re.search(r'(?:runde|handy)\s*(\d+)', descr, re.IGNORECASE | re.MULTILINE)
#     return int(matches[1]) if matches else None


class ShiftStream(ShiftbaseStream):
    name = 'shift'
    path = '/shifts'
    schema_filepath = SCHEMAS_DIR / 'shift.json'
    primary_keys = ['id']
    records_jsonpath = '$.data[*].Shift'

    def get_url_params(self, context: dict | None, next_page_token: _TToken | None) -> dict[str, Any]:
        return dict(
            allow_deleted=True
        )


class UserStream(ShiftbaseStream):
    name = 'user'
    path = '/users'
    schema_filepath = SCHEMAS_DIR / 'user.json'
    primary_keys = ['id']
    records_jsonpath = '$.data[*].User'
