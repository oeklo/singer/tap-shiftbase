from tap_shiftbase.streams import parse_route_number


def test_parse_route_number():
    test_data = {
        'Servicerunde 2\nVW BUS\n\nBaustellen, Lobau, Wolkersdorf': 2,
        'Fässer & öKlos Waschen': None,
        'Iveco 1': None,
        'Service Runde 3\nIveco 2\n\nWest Runde': 3,
        'Iveco 2\nHandy 8\n': 8,
    }
    for descr, route_number in test_data.items():
        assert parse_route_number(descr) == route_number


